/**
 * Chunk Page
 *
 * Controller for the a chunk page
 */

Template.chunk.helpers({
  debts: function() {
    var debt_ids = this.debts;
    
    //return all the associated debt documents
    //in the collection Debts.. ie 
    //return debts.find(...)
  } ,
  
  expenses: function() {
    var expense_ids = this.expenses;

    //return all the expense documents
    //from the collection Expenses
    //return Expenses.find(..)
  }
});

Template.chunk.events = { 

  'submit #add-expense': function(e, t) {
    e.preventDefault();
    
    //RETRIEVE THE FORM FIELD VALUES
    //var person = t.find(....)

    //INSERT INTO EXPENSES COLLECTION
    //Expenses.insert({...})
    
    // Recalculate Debts && update current chunk data in the server
    //"this" will refer to the current chunk, so you can use that to 
    //update the chunk eg this.description will be the description of th
    //current chunk. this._id will be the id of the current chunk
    //
    //New chunk data will be reflected in the chunk page automatically once
    //the chunk is updated on the server
  }
}

Template.expense_item.helpers({
  participant_names: function() {
    var participant_ids = this.participants;
  
    //return all names of the participants
    //return People.find(..).name
  }
})

  
