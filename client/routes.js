/**
 * This will be the routes file
 */

/* -------------------  CONFIGURATIONS  -------------------- */
Router.configure({
   layoutTemplate: 'layout' 
})

/* -------------------  BEFORE HOOKS -------------------- */

Router.onBeforeAction();

/* -------------------  ROUTES  -------------------- */

Router.map(function() {
  /*
   * HOME
   */
  this.route('home', {
    path: '/',
    yieldTemplates: {
      'nav': {to: 'nav'},
      'home' : {to: 'content'}
    },
    data: function() {
      return {chunks: Chunks.find()}; //return all the chunks in the database
    }
  });

  /*
   * CHUNK PAGE
   */
  this.route('chunk', {
    path: '/chunk/:chunkID',
    yieldTemplates: {
      'nav': {to: 'nav'},
      'chunk' : {to: 'content'}
    },
    //Return the document from Chunks with _id chunkID
    data: function() {
      return Chunks.findOne({ _id: this.params.chunkID});
    }
  });
});
