
Chunks = new Meteor.Collection("chunk");

ChunkSchema = new SimpleSchema({
  expenses: {
    type: [Number], //array of Expense IDs associated with this chunk
    label: "Expenses"
  },
  start_date:{
    type: Date,
    label: "Start Date"
  },
  end_date:{
    type: Date,
    label: "End Date"
  },
  debts:{
    type:[Number], //array of IDs to debt objects
    label: "Debts"
  }
})

Chunks.attachSchema(ChunkSchema);
