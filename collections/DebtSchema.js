Debts = new Meteor.Collection("debts");

DebtSchema = new SimpleSchema({
  to: {
    type: Number,  //ID of the person to whom this debt should be paid
    label: "To"
  },
  from:{
    type: Number, //ID of the person who must pay this debt
    label:"From"
  },
  amount: {
    type: Number, //amount in dollars owed
    label: "Amount owed"
  },
  paid: {
    type: Boolean, //whether this debt has been paid
  },
})

Debts.attachSchema(DebtSchema);
