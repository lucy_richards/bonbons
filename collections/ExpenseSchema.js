
Expenses = new Meteor.Collection("expenses");

ExpensesSchema = new SimpleSchema({
  submitted:{
    type: Number, //The time this item was submitted (UTC)
    label: "Submitted at",
  },
  payer:{
    type: Number, //The ID number of the person who paid
    label: "Paid by"
  },
  paid:{
    type: Number, //The amount paid by the payer
    label: "Amount paid"
  },
  description:{
    type: String, //The description of this payment
    label: "Description"
  },
  participants:{
    type: [Number], //an array of Person IDs who participated in this expense
    label: "Participants"
  }
});

Expenses.attachSchema(ExpensesSchema);
