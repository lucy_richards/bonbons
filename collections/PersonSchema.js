People = new Meteor.Collection("people")

PersonSchema = new SimpleSchema({
  name: {
    type: String,
    label: "Name"
  }
});

People.attachSchema(PersonSchema);

